from System.Collections.Generic import List
from System import Byte, Int32
#
# Lockpick nearby chest
#
LootListName = "chest"
PickableChests = List[int]([ 0x0E40, 0x0E41, 0x09AB, 0x0E42, 0x0E43, 0x0E77, 0x0E7C, 0x0E7E, 0x09A9, 0x0E7F, 0x0E3E, 0x0E3D, 0x0E3F, 0x0E3C])
# 
# You need an autoloot list with the name that matches whatever you specify in LootListName above
# PickableChests is probably everything, but if you find something it doesn't recognize as pickable
# add it to the list 
#
#
chest = Items.Filter()
chest.Enabled = True
chest.OnGround = True
chest.Movable = False
chest.Graphics = PickableChests
chest.RangeMax = 1
chestOnGround = Items.ApplyFilter(chest)
#
LootList = set()
for item in AutoLoot.GetList(LootListName):
    LootList.add(item.Graphics)
#
def disarm_trap(chest):
    Journal.Clear()    
    if Player.GetRealSkillValue("Remove Trap") > 50:
        finished = False
        while not finished:         
            Player.UseSkill("Remove Trap")
            Target.WaitForTarget(3000)
            Target.TargetExecute(chest.Serial)
            Misc.Pause(6000)
            if Journal.Search('You successfully render'):
                finished = True
            elif Journal.Search("doesn't appear to be trapped"): 
                finished = True
            else:    
                Misc.Pause(6000)
#    
for chest in chestOnGround:
    Journal.Clear()
    disarm_trap(chest)
    pick = Items.FindByID(0x14FC, -1, Player.Backpack.Serial)
    if pick != None:
        Items.UseItem(pick)
        Target.WaitForTarget(5000, False)
        Target.TargetExecute(chest)
        Misc.Pause(1000)
        if Journal.Search("not appear to be locked"):
            pass
        else:
            Misc.Pause(4000)    
        Items.UseItem(chest)
    Misc.Pause(1000)
    Items.UseItem(chest)
    Misc.Pause(1000)
    lootBag = Player.Backpack.Serial
    if Misc.CheckSharedValue("LootBag"):
        lootBag = Misc.ReadSharedValue("LootBag")
    for item in chest.Contains:
        name = Items.GetPropStringByIndex(item, 0)
        #Misc.SendMessage(str(name))
        if item.ItemID in LootList or "gargoyle" in name.lower():
            Items.Move(item, lootBag, 0)
            Misc.Pause(1000)
        if "Dungeon Chest Loot" in Items.GetPropStringList(item):
            Items.Move(item, lootBag, 0)
            Misc.Pause(1000)
            
